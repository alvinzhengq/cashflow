const mongoose = require('mongoose')

const User = new mongoose.Schema({
    username: {type:String, trim:true, default:''},
    passwordHash: {type:String, trim:true, default:''},
})

module.exports.User = mongoose.models['Users'] || mongoose.model('Users', User);