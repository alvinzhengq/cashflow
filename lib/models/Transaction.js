
module.exports = class Transaction {
    constructor(queryObj) {
        const { to, from, amount, date } = queryObj;
        
        this.to = to;
        this.from = from;
        this.amount = amount;
        this.date = date;
    }
}