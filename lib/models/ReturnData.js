
module.exports = class ReturnData {
    constructor(data, good) {        
        this.data = data;
        this.good = good;
    }

    getJSON() {
        return {status: (this.good ? "GOOD" : "ERROR"), msg: this.data}
    }
}