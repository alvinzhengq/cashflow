const mongoose = require('mongoose')

const UserData = new mongoose.Schema({
    username: {type:String, trim:true, default:''},
    balance: {type:Number, default:0},
    transactionHistory: {type:Array, default:[]},
    transferRequests: {type:Array, default: []}
})

module.exports.UserData = mongoose.models['UserData'] || mongoose.model('UserData', UserData);