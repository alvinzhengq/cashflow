
module.exports = class Request {
    constructor(queryObj) {
        const {to, from, amount, description, id} = queryObj;
        
        this.to = to;
        this.from = from;
        this.amount = amount;
        this.description = description;
        this.id = id;
    }
}