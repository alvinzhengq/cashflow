
import { createContext, useContext, useState } from 'react';

export const authenticationContext = createContext();

export function AuthWrapper({ children }) {
    const [ userData, setUserData ] = useState({});
    const [ token, setToken ] = useState("");

    let sharedState = {
        userData, setUserData,
        token, setToken
    }

    return (
        <authenticationContext.Provider value={sharedState}>
            { children }
        </authenticationContext.Provider>
    )
}

export function useAuthContext() {
    return useContext(authenticationContext);
}