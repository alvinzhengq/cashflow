
module.exports.USERNAME_IS_EMPTY = 'USERNAME_IS_EMPTY'
module.exports.USERNAME_IS_WRONG_FORMAT = 'USERNAME_IS_WRONG_FORMAT'
module.exports.PASSWORD_IS_EMPTY = 'PASSWORD_IS_EMPTY';
module.exports.WRONG_PASSWORD = 'WRONG_PASSWORD';
module.exports.SOME_THING_WENT_WRONG = 'SOME_THING_WENT_WRONG';
module.exports.USER_DOES_NOT_EXIST = 'USER_DOES_NOT_EXIST';
module.exports.TOKEN_IS_EMPTY = 'TOKEN_IS_EMPTY';
module.exports.INVALID_TOKEN = 'INVALID_TOKEN';

module.exports.INVALID_BALANCE = 'INVALID_BALANCE';
module.exports.TRANSACTION_FAILED = 'TRANSACTION_FAILED';
module.exports.TRANSACTION_SUCCESS = 'TRANSACTION_SUCCESS';
module.exports.REQUEST_SUCCESS = 'REQUEST_SUCCESS';
module.exports.REQUEST_FAILED = 'REQUEST_FAILED';
module.exports.REQUEST_DEL_SUCCESS = 'REQUEST_DEL_SUCCESS';
module.exports.REQUEST_DEL_FAILED = 'REQUEST_DEL_FAILED';

module.exports.REMOTE_URL = (process.env.NODE_ENV !== 'production') ? 'http://localhost:3000' : 'http://161.35.237.57';
module.exports.secretKey = process.env.SECRET_KEY;
