const axios = require('axios').default
import { REMOTE_URL } from "./constants"

export let checkToken = async () => {
    let token = localStorage.getItem("token");
    if (!token) {
        return false
    } else {
        let res = await axios.post(REMOTE_URL + "/api/auth/check", {
            jwt: token
        })
        
        return res.data.msg
    }
}

export let login = async (username, password) => {
    let res = await axios.post(REMOTE_URL + "/api/auth/login", {
        username: username,
        password: password
    })

    return res.data
}

export let timeout = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export let getData = async (jwt) => {
    let res = await axios.post(REMOTE_URL + "/api/data/get", {
        jwt: jwt
    })

    return res.data.msg
}

export let preformTransaction = async (jwt, to, amount) => {
    let res = await axios.post(REMOTE_URL + "/api/data/transaction", {
        jwt: jwt,
        to: to,
        amount: amount
    })

    return res.data.msg
}

export let submitRequest = async (jwt, to, amount, description) => {
    let res = await axios.post(REMOTE_URL + "/api/data/request-add", {
        jwt: jwt,
        to: to,
        amount: (isNaN(amount) ? 0 : amount),
        description: description
    })

    return res.data.msg
}

export let deleteRequest = async (jwt, id) => {
    let res = await axios.post(REMOTE_URL + "/api/data/request-del", {
        jwt: jwt,
        id: id
    })

    return res.data.msg
}