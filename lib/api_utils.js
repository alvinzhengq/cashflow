const crypto = require('crypto')
const jwt = require('jsonwebtoken')
const { check } = require('express-validator');
const { UserData } = require("./models/UserData")
const {
    PASSWORD_IS_EMPTY,
    USERNAME_IS_EMPTY,
    USERNAME_IS_WRONG_FORMAT,
    TOKEN_IS_EMPTY,
    INVALID_TOKEN,
    secretKey
} = require('./constants')

module.exports.generateHashedPassword = password => crypto.createHash('sha256').update(password).digest('hex');

module.exports.randomID = () => crypto.randomUUID()

module.exports.validateJWT = jwtToken => {
    try {
        let curToken = jwt.verify(jwtToken, secretKey)
        if (curToken.exp < Math.floor(Date.now() / 1000)) return false;

        return curToken;
    }
    catch { return false }
}

module.exports.newJWT = (username) => jwt.sign({
    name: username,
    exp: Math.floor(Date.now() / 1000) + 2592000
}, secretKey)

module.exports.preformTransaction = (curTransaction) => {
    UserData.findOneAndUpdate({ username: curTransaction.to }, { $inc: { balance: curTransaction.amount }, $push: { transactionHistory: curTransaction } }, { new: true })
        .then(profile => console.log(profile))
        .catch(err => console.log(err))

    UserData.findOneAndUpdate({ username: curTransaction.from }, { $inc: { balance: -curTransaction.amount }, $push: { transactionHistory: curTransaction } }, { new: true })
        .then(profile => console.log(profile))
        .catch(err => console.log(err))
}

module.exports.initMiddleware = (middleware) => {
    return (req, res) =>
        new Promise((resolve, reject) => {
            middleware(req, res, (result) => {
                if (result instanceof Error) {
                    return reject(result)
                }
                return resolve(result)
            })
        })
}

module.exports.validateMiddleware = (validations, validationResult) => {
    return async (req, res, next) => {
        await Promise.all(validations.map((validation) => validation.run(req)))

        const errors = validationResult(req)
        if (errors.isEmpty()) {
            return next()
        }

        res.status(200).json({ errors: errors.array() })
    }
}

module.exports.loginValidation = [
    check('username')
        .exists()
        .withMessage(USERNAME_IS_EMPTY)
        .isString()
        .withMessage(USERNAME_IS_WRONG_FORMAT),
    check('password')
        .exists()
        .withMessage(PASSWORD_IS_EMPTY)
];

module.exports.dataRequestValidation = [
    check('jwt')
        .exists()
        .withMessage(TOKEN_IS_EMPTY)
        .isString()
        .withMessage(INVALID_TOKEN),
];

module.exports.transactionValidation = [
    check('jwt')
        .exists()
        .withMessage(TOKEN_IS_EMPTY)
        .isString()
        .withMessage(INVALID_TOKEN),
    check('to')
        .exists()
        .isString(),
    check('amount')
        .exists()
        .isInt({ min: 1, max: 10e9 })
];

module.exports.requestValidation = [
    check('jwt')
        .exists()
        .withMessage(TOKEN_IS_EMPTY)
        .isString()
        .withMessage(INVALID_TOKEN),
    check('to')
        .exists()
        .isString(),
    check('amount')
        .exists()
        .isInt({ min: 0, max: 10e9 }),
    check('description')
        .exists()
        .isString()
        .isLength({ max: 1000 })
];

module.exports.requestDelValidation = [
    check('jwt')
        .exists()
        .withMessage(TOKEN_IS_EMPTY)
        .isString()
        .withMessage(INVALID_TOKEN),
    check('id')
        .exists()
        .isString(),
];
