
import * as React from 'react'
import '../styles/globals.css'
import { PlasmicRootProvider } from '@plasmicapp/react-web';
import { registerComponent } from '@plasmicapp/host'

import { Alert } from '@mui/material';
import { StyledTextField } from '../components/StyledTextField'
import { RequestItem } from '../components/RequestItem'

import { AuthWrapper } from '../lib/authContext'
import { motion, AnimatePresence } from "framer-motion";
import { useRouter, Router } from 'next/router'

import Head from 'next/head'

const routeChange = () => {
  // Temporary fix to avoid flash of unstyled content
  // during route transitions. Keep an eye on this
  // issue and remove this code when resolved:
  // https://github.com/vercel/next.js/issues/17464

  const tempFix = () => {
    const allStyleElems = document.querySelectorAll('style[media="x"]');
    allStyleElems.forEach((elem) => {
      elem.removeAttribute("media");
    });
  };
  tempFix();
};
Router.events.on("routeChangeComplete", routeChange);
Router.events.on("routeChangeStart", routeChange);

function MyApp({ Component, pageProps }) {
  const router = useRouter();

  registerComponent(Alert, {
    name: 'Alert',
    props: {
      children: 'slot',
      variant: 'string',
      severity: 'string',
    },
    importPath: '@mui/material'
  })

  registerComponent(StyledTextField, {
    name: 'StyledTextField',
    props: {
      children: 'slot',
      label: 'string',
      variant: 'string',
      type: 'string',
      placeholder: 'string',
      multiline: 'boolean',
      rows: 'number'
    },
    importPath: './components/StyledTextField'
  })

  registerComponent(RequestItem, {
    name: 'RequestItem',
    props: {
      cash: "string",
      description: "string",
      requestName: "string"
    },
    importPath: './components/RequestItem'
  })

  React.useEffect(() => {
    router.push(router.asPath);
  }, []);

  return (
    <PlasmicRootProvider>
      <Head>
        <title>CashFlow</title>
      </ Head>
      <AnimatePresence exitBeforeEnter >
        <motion.div
          key={router.asPath}
          initial={{ scale: 0.97, opacity: 0 }}
          animate={{ scale: 1, opacity: 1 }}
          exit={{ scale: 0.97, opacity: 0 }}
          transition={{
            duration: 0.24,
            type: 'tween',
            ease: 'easeInOut'
          }}
        >
          <AuthWrapper>
            <Component {...pageProps} />
          </AuthWrapper>
        </motion.div>
      </AnimatePresence>
    </PlasmicRootProvider>
  )
}

export default MyApp
