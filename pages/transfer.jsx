
import * as React from "react";
import { PlasmicTransferRequest } from "../components/plasmic/cash_flow/PlasmicTransferRequest";
import { RequestItem } from "../components/RequestItem";
import { useAuthContext, authenticationContext } from '../lib/authContext'
import { REMOTE_URL } from '../lib/constants'
import Cookies from 'cookies'
import { AnimatePresence, AnimateSharedLayout, motion } from 'framer-motion'


const axios = require('axios').default

export async function getServerSideProps({ req, res }) {
  const cookies = new Cookies(req, res)

  if (cookies.get('jwt') === undefined) {
    return {
      redirect: {
        destination: '/login',
        permanent: false
      }
    }
  }

  let jwtCheck = await axios.post(`${REMOTE_URL}/api/auth/check`, {
    jwt: cookies.get('jwt')
  })

  if (!jwtCheck.data.msg) {
    return {
      redirect: {
        destination: '/login',
        permanent: false
      }
    }
  }

  let userData = await axios.post(`${REMOTE_URL}/api/data/get`, {
    jwt: cookies.get('jwt')
  })

  return {
    props: { userData: userData.data.msg },
  }
}

function TransferRequest({ userData }) {
  const authData = useAuthContext();
  if (Object.keys(authData.userData).length === 0) {
    authData.setUserData(userData)
  }

  const [alertList, setAlerts] = React.useState([]);
  const interval = React.useRef(null);
  const ids_length = React.useRef(null);

  const variants = {
    pre: { opacity: 0 },
    visible: { opacity: 1 }
  };

  function set_remove_ids_interval() {
    if (!interval.current) {
      interval.current = setInterval(() => {
        if (ids_length.current === 0) {
          clearInterval(interval.current);
          interval.current = null;
          return;
        }
        ids_length.current = ids_length.current - 1;
        setAlerts(([first, ...others]) => [first, ...others].splice(0, ids_length.current));
        set_remove_ids_interval();
      }, 2000);
    }
  }

  function add(item) {
    if (ids_length.current > 3) return;

    ids_length.current = alertList.length + 1;
    setAlerts([...alertList, (
      <motion.div
        layout
        variants={variants}
        initial="pre"
        animate="visible"
        key={Math.random()}
      >
        {item}
      </motion.div>
    )]);
    set_remove_ids_interval();
  }

  let [transferData, setTransferData] = React.useState([])
  React.useEffect(() => {
    if (authData.userData.transferRequests) {
      setTransferData(authData.userData.transferRequests.map((request, index) =>
        <RequestItem
          key={Math.random()}
          requestName={request.from}
          cash={request.amount}
          description={request.description}
          id={request.id}
          add={add}
          setUserData={authData.setUserData}
        />)
      )
    }
  }, [authData.userData.transferRequests])

  React.useEffect(() => {
    return () => {
      if (interval.current) clearInterval(interval.current);
    };
  }, []);

  return (
    <authenticationContext.Consumer>
      {
        context =>
          <PlasmicTransferRequest
            sideMenu={{
              props: {
                username: userData.username
              }
            }}

            requestCard={{
              props: {
                add: add
              }
            }}

            transferCard={{ props: { requests: transferData } }}

            alertList={
              <AnimateSharedLayout>
                <motion.div>
                  <AnimatePresence>
                    {alertList}
                  </AnimatePresence>
                </motion.div>
              </AnimateSharedLayout>
            }
          />
      }
    </authenticationContext.Consumer>
  )
}

export default TransferRequest;
