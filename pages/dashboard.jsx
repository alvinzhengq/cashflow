
import * as React from 'react'
import { PlasmicDashboard } from "../components/plasmic/cash_flow/PlasmicDashboard";
import Transaction from "../components/Transaction";
import { useAuthContext } from '../lib/authContext'
import { REMOTE_URL } from '../lib/constants'
import { useRouter } from 'next/router'
import Cookies from 'cookies'

const axios = require('axios').default

export async function getServerSideProps({ req, res }) {
  const cookies = new Cookies(req, res)
  
  if (cookies.get('jwt') === undefined) {
    return {
      redirect: {
        destination: '/login',
        permanent: false
      }
    }
  }
  
  let jwtCheck = await axios.post(`${REMOTE_URL}/api/auth/check`, {
    jwt: cookies.get('jwt')
  })
  
  if (!jwtCheck.data.msg) {
    return {
      redirect: {
        destination: '/login',
        permanent: false
      }
    }
  }
  
  let userData = await axios.post(`${REMOTE_URL}/api/data/get`, {
    jwt: cookies.get('jwt')
  })
  
  return {
    props: { userData: userData.data.msg },
  }
}

function Dashboard({ userData }) {
  const router = useRouter()
  const authData = useAuthContext();
  authData.setUserData(userData)

  let expense = 0, income = 0;
  let startOfMonth = Math.floor(new Date(new Date().getFullYear(), new Date().getMonth()) / 1000);

  for (let item of userData.transactionHistory) {
    if (item.date >= startOfMonth) {
      if (item.to === userData.username) income += item.amount;
      else expense += item.amount;
    }
  }

  const transactionStyle = {
    marginTop: "1em",
    marginBottom: "1em",
    width: "100%"
  }

  return <PlasmicDashboard
    balanceCard={{
      props: {
        title: `Hello ${userData.username}, \nYour Current Balance Total is:`,
        balanceNumber: `${(userData.balance >= 1000 ? (userData.balance / 1000)+"k" : userData.balance)} CA$H`,
        button: {
          onClick: (e) => router.push("/transfer")
        }
      }
    }}

    historyCard={{
      props: {
        transactionList: userData.transactionHistory.slice(userData.transactionHistory.length - 4, userData.transactionHistory.length).reverse().map((transaction) =>
          <Transaction
            key={Math.random()}
            transactionText={`Transfer ${transaction.to != userData.username ? "to " + transaction.to : "from " + transaction.from}`}
            transactionAmount={"$" + transaction.amount} style={transactionStyle}
            outgoing={transaction.to != userData.username}
          />
        )
      }
    }}

    expenseCard={{
      props: {
        data: expense
      }
    }}

    incomeCard={{
      props: {
        data: income
      }
    }}

    sideMenu={{
      props:{
        username: userData.username
      }
    }}
  />;
}

export default Dashboard;
