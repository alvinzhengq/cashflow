
import * as sty from '../styles/WIP.module.css'
import Link from 'next/link'
import Cookies from 'cookies'
import { REMOTE_URL } from '../lib/constants'

const axios = require('axios').default

export async function getServerSideProps({ req, res }) {
  const cookies = new Cookies(req, res)
  
  if (cookies.get('jwt') === undefined) {
    return {
      redirect: {
        destination: '/login',
        permanent: false
      }
    }
  }
  
  let jwtCheck = await axios.post(`${REMOTE_URL}/api/auth/check`, {
    jwt: cookies.get('jwt')
  })
  
  if (!jwtCheck.data.msg) {
    return {
      redirect: {
        destination: '/login',
        permanent: false
      }
    }
  }
  
  return {
    props: {},
  }
}

export default function Store() {
    return (
        <div className={sty.container}>
            <img src="/shovel.svg" className={sty.svgImage}></img>
            <h1 className={sty.title}>Under Construction</h1>
            <Link href="/dashboard"><h4 className={sty.link}>Return to Dashboard</h4></Link>
        </div>
    )
}