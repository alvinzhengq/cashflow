
import * as React from "react";
import { PlasmicLogin } from "../components/plasmic/cash_flow/PlasmicLogin";
import { Alert, AlertTitle } from '@mui/material'
import { useAuthContext } from '../lib/authContext'
import { REMOTE_URL } from '../lib/constants'
import { login } from '../lib/utils'
import Cookies from 'cookies'
import Router from 'next/router'
import { AnimatePresence, AnimateSharedLayout, motion } from 'framer-motion'

const axios = require('axios').default

export async function getServerSideProps({ req, res }) {
  const cookies = new Cookies(req, res)

  if (cookies.get('jwt') === undefined) {
    return {
      props: {}
    }
  }

  let jwtCheck = await axios.post(`${REMOTE_URL}/api/auth/check`, {
    jwt: cookies.get('jwt')
  })

  if (!jwtCheck.data.msg) {
    return {
      props: {}
    }
  }

  return {
    redirect: {
      destination: '/dashboard',
      permanent: false
    }
  }
}

const Login = () => {
  const authData = useAuthContext();

  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");

  const [alertList, setAlerts] = React.useState([]);
  const interval = React.useRef(null);
  const ids_length = React.useRef(null);

  const variants = {
    pre: { opacity: 0 },
    visible: { opacity: 1 }
  };

  function set_remove_ids_interval() {
    if (!interval.current) {
      interval.current = setInterval(() => {
        if (ids_length.current === 0) {
          clearInterval(interval.current);
          interval.current = null;
          return;
        }
        ids_length.current = ids_length.current - 1;
        setAlerts(([first, ...others]) => [first, ...others].splice(0, ids_length.current));
        set_remove_ids_interval();
      }, 1600);
    }
  }

  function add(item) {
    ids_length.current = alertList.length + 1;
    setAlerts([...alertList, item]);
    set_remove_ids_interval();
  }

  React.useEffect(() => {
    return () => {
      if (interval.current) clearInterval(interval.current);
    };
  }, []);

  return <PlasmicLogin
    username={{
      props: {
        onChange: (e) => setUsername(e.target.value)
      }
    }}

    password={{
      props: {
        onChange: (e) => setPassword(e.target.value)
      }
    }}

    submit={{
      props: {
        onClick: async (e) => {
          let authed = await login(username, password)

          if (authed.status == 'GOOD') {
            authData.setToken(authed.msg)
            document.cookie = `jwt=${authed.msg}; expires=Fri, 31 Dec 9999 23:59:59 GMT`
            Router.push('/dashboard')

          } else {
            add(
              <motion.div
                layout
                variants={variants}
                initial="pre"
                animate="visible"
                key={Math.random()}
              >
                <Alert variant="standard" severity="error" style={{ margin: "1em" }}>
                  <AlertTitle>Invalid Username or Password</AlertTitle>
                </Alert>
              </motion.div>
            )
          }

        }
      }
    }}

    alertList={
      <AnimateSharedLayout>
        <motion.div>
          <AnimatePresence>
            {alertList}
          </AnimatePresence>
        </motion.div>
      </AnimateSharedLayout>
    }
  />;
}

export default Login;
