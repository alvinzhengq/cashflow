
const { User } = require('../../../lib/models/User')
const { UserData } = require('../../../lib/models/UserData')
const crypto = require('crypto')

import dbConnect from '../../../lib/dbConnect'

export default async function handler(req, res) {
    let csvFile = crypto.randomBytes(16).toString('hex')
    await require('fs').writeFile(`/tmp/${csvFile}.csv`, '', ()=>{})
    
    const createCsvWriter = require('csv-writer').createObjectCsvWriter;
    const csvWriter = createCsvWriter({
        path: `/tmp/${csvFile}.csv`,
        header: [
            { id: 'name', title: 'Name' },
            { id: 'bal', title: 'Balance' }
        ]
    });

    await dbConnect();

    let masterUserData = await UserData.find({}).sort('-balance');
    let allRecords = []

    for(let item of masterUserData) {
        let tempObj = {}
        tempObj['name'] = item.username;
        tempObj['bal'] = item.balance;

        allRecords.push(tempObj)
    }

    await csvWriter.writeRecords(allRecords)
    res.setHeader('Content-Type', 'text/csv')
    res.setHeader('Content-disposition', 'attachment; filename=' + `balance.csv`);
    res.send(require('fs').readFileSync(`/tmp/${csvFile}.csv`))
}