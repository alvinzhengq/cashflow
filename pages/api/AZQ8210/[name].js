
const { User } = require('../../../lib/models/User')
const { UserData } = require('../../../lib/models/UserData')
const crypto = require('crypto')

import dbConnect from '../../../lib/dbConnect'

function containsObject(obj, list) {
    var x;
    for (x in list) {
        if (list.hasOwnProperty(x) && list[x] === obj) {
            return true;
        }
    }

    return false;
}

export default async function handler(req, res) {
    const { name } = req.query

    await dbConnect();
    let exists = await UserData.exists({username: name})
    if(!exists) {
        res.status(200).send("No Such User Exists")
        return
    }

    let csvFile = crypto.randomBytes(16).toString('hex')
    await require('fs').writeFile(`/tmp/${csvFile}.csv`, '', ()=>{})

    const createCsvWriter = require('csv-writer').createObjectCsvWriter;
    const csvWriter = createCsvWriter({
        path: `/tmp/${csvFile}.csv`,
        header: [
            { id: 'to', title: 'To' },
            { id: 'from', title: 'From' },
            { id: 'amt', title: 'Amount' },
            { id: 'date', title:  'Date'}
        ]
    });


    let masterUserData = await UserData.find({ username: name });
    let allRecords = []
    let seen = []

    for (let item of masterUserData) {
        for (let transaction of item.transactionHistory) {
            let tempObj = {}

            tempObj.to = transaction.to;
            tempObj.from = transaction.from;
            tempObj.amt = transaction.amount;
            tempObj.date = new Date(parseInt(transaction.date)*1000).toLocaleDateString("en-US") + " " + new Date(parseInt(transaction.date)*1000).toLocaleTimeString("en-US")
            
            if (!seen.includes(JSON.stringify(transaction))) {
                allRecords.push(tempObj)
                seen.push(JSON.stringify(transaction))
            }
        }
    }


    await csvWriter.writeRecords(allRecords)
    res.setHeader('Content-Type', 'text/csv')
    res.setHeader('Content-disposition', 'attachment; filename=' + `${name}.csv`);
    res.send(require('fs').readFileSync(`/tmp/${csvFile}.csv`))
}