
const { validationResult } = require('express-validator')
const { validateJWT, dataRequestValidation, transactionValidation, preformTransaction, requestValidation, requestDelValidation, randomID, initMiddleware, validateMiddleware } = require('../../../lib/api_utils')
const { User } = require('../../../lib/models/User')
const { UserData } = require('../../../lib/models/UserData')
const ReturnData = require('../../../lib/models/ReturnData')
const Request = require('../../../lib/models/Request')

const {
    INVALID_TOKEN,
    USER_DOES_NOT_EXIST,
    INVALID_BALANCE,
    TRANSACTION_SUCCESS,
    TRANSACTION_FAILED,
    REQUEST_SUCCESS,
    REQUEST_FAILED,
    REQUEST_DEL_SUCCESS,
    REQUEST_DEL_FAILED
} = require('../../../lib/constants');
import dbConnect from '../../../lib/dbConnect'

const validateBody = initMiddleware(
    validateMiddleware(requestDelValidation, validationResult)
)

export default async function handler(req, res) {
    if (req.method !== 'POST') res.status(400).json(new ReturnData("Invalid Request", false).getJSON())

    await validateBody(req, res)
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        res.status(400).json(new ReturnData("Invalid Request", false).getJSON())
    }

    /* --------------------------------------------------------------------------------------------------------------------------------- */

    await dbConnect();

    let curToken = validateJWT(req.body.jwt)
    if (curToken) {
        let profile = await UserData.findOneAndUpdate({ username: curToken.name }, { $pull: { transferRequests: { id: req.body.id } } }, { new: false })
        for (let item of profile.transferRequests) {
            if (item.id === req.body.id) {
                res.status(200).json(new ReturnData(REQUEST_DEL_SUCCESS, true).getJSON())
                return
            }
        }

        res.json(new ReturnData(REQUEST_DEL_FAILED, false).getJSON())

    } else {
        res.json(new ReturnData(INVALID_TOKEN, false).getJSON())
        
    }
}