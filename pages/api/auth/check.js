
const { validationResult } = require('express-validator')
const { loginValidation, generateHashedPassword, dataRequestValidation, newJWT, validateJWT, initMiddleware, validateMiddleware } = require('../../../lib/api_utils')
const { User } = require('../../../lib/models/User')
const ReturnData = require('../../../lib/models/ReturnData')

const {
    USER_DOES_NOT_EXIST
} = require('../../../lib/constants');
import dbConnect from '../../../lib/dbConnect'

const validateBody = initMiddleware(
    validateMiddleware(dataRequestValidation, validationResult)
)

export default async function handler(req, res) {
    if (req.method !== 'POST') res.status(400).json(new ReturnData("Invalid Request", false).getJSON())

    await validateBody(req, res)
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        res.status(400).json(new ReturnData("Invalid Request", false).getJSON())
    }

    /* --------------------------------------------------------------------------------------------------------------------------------- */

    if(validateJWT(req.body.jwt)) {
        res.json(new ReturnData(true, true).getJSON())

    } else {
        res.json(new ReturnData(false, true).getJSON())
        
    }
}