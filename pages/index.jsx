import { title } from '../styles/Home.module.css'
import * as React from 'react'

export async function getServerSideProps({ req, res }) {
    return {
        redirect: {
            destination: '/login',
            permanent: false
        }
    }
}

const Homepage = ({ userObj }) => {
    return <h1 className={title}>Placeholder</h1>
}

export default Homepage;