// @ts-nocheck
/* eslint-disable */
/* tslint:disable */
/* prettier-ignore-start */
/** @jsxRuntime classic */
/** @jsx createPlasmicElementProxy */
/** @jsxFrag React.Fragment */
// This class is auto-generated by Plasmic; please do not edit!
// Plasmic Project: cqaN2hQVYn9VYyCgFwJ8d
// Component: dEMGDJHgA9
import * as React from "react";
import * as p from "@plasmicapp/react-web";
import {
  classNames,
  createPlasmicElementProxy,
  deriveRenderOpts
} from "@plasmicapp/react-web";
import { StyledTextField } from "../../StyledTextField"; // plasmic-import: 59MTcjqjs3/codeComponent
import Button from "../../Button"; // plasmic-import: CeWld3oJrL/component
import "@plasmicapp/react-web/lib/plasmic.css";
import * as projectcss from "./plasmic_cash_flow.module.css"; // plasmic-import: cqaN2hQVYn9VYyCgFwJ8d/projectcss
import * as sty from "./PlasmicRequestCard.module.css"; // plasmic-import: dEMGDJHgA9/css

export const PlasmicRequestCard__VariantProps = new Array();

export const PlasmicRequestCard__ArgProps = new Array();

function PlasmicRequestCard__RenderFunc(props) {
  const { variants, args, overrides, forNode, dataFetches } = props;
  return (
    <div
      data-plasmic-name={"root"}
      data-plasmic-override={overrides.root}
      data-plasmic-root={true}
      data-plasmic-for-node={forNode}
      className={classNames(projectcss.all, projectcss.root_reset, sty.root)}
    >
      <p.Stack
        as={"div"}
        data-plasmic-name={"freeBox"}
        data-plasmic-override={overrides.freeBox}
        hasGap={true}
        className={classNames(projectcss.all, sty.freeBox)}
      >
        <div
          data-plasmic-name={"title"}
          data-plasmic-override={overrides.title}
          className={classNames(
            projectcss.all,
            projectcss.__wab_text,
            sty.title
          )}
        >
          {"Open a New CA$H Request"}
        </div>

        <StyledTextField
          data-plasmic-name={"name"}
          data-plasmic-override={overrides.name}
          className={classNames("__wab_instance", sty.name)}
          label={"Username"}
          placeholder={"John Doe"}
          type={""}
        />

        <StyledTextField
          data-plasmic-name={"cashAmount"}
          data-plasmic-override={overrides.cashAmount}
          className={classNames("__wab_instance", sty.cashAmount)}
          label={"Hours of Work Done"}
          placeholder={"ex. 4"}
          type={"number"}
        />

        <StyledTextField
          data-plasmic-name={"description"}
          data-plasmic-override={overrides.description}
          className={classNames("__wab_instance", sty.description)}
          label={"Request Description"}
          multiline={true}
          placeholder={"Example: Reason for Request, Nature of Work Done"}
          rows={6}
        />

        <Button
          data-plasmic-name={"submit"}
          data-plasmic-override={overrides.submit}
          className={classNames("__wab_instance", sty.submit)}
        >
          {"Submit Request"}
        </Button>
      </p.Stack>
    </div>
  );
}

const PlasmicDescendants = {
  root: [
    "root",
    "freeBox",
    "title",
    "name",
    "cashAmount",
    "description",
    "submit"
  ],

  freeBox: ["freeBox", "title", "name", "cashAmount", "description", "submit"],
  title: ["title"],
  name: ["name"],
  cashAmount: ["cashAmount"],
  description: ["description"],
  submit: ["submit"]
};

function makeNodeComponent(nodeName) {
  const func = function (props) {
    const { variants, args, overrides } = deriveRenderOpts(props, {
      name: nodeName,
      descendantNames: [...PlasmicDescendants[nodeName]],
      internalArgPropNames: PlasmicRequestCard__ArgProps,
      internalVariantPropNames: PlasmicRequestCard__VariantProps
    });

    const { dataFetches } = props;
    return PlasmicRequestCard__RenderFunc({
      variants,
      args,
      overrides,
      dataFetches,
      forNode: nodeName
    });
  };
  if (nodeName === "root") {
    func.displayName = "PlasmicRequestCard";
  } else {
    func.displayName = `PlasmicRequestCard.${nodeName}`;
  }
  return func;
}

export const PlasmicRequestCard = Object.assign(
  // Top-level PlasmicRequestCard renders the root element
  makeNodeComponent("root"),
  {
    // Helper components rendering sub-elements
    freeBox: makeNodeComponent("freeBox"),
    title: makeNodeComponent("title"),
    _name: makeNodeComponent("name"),
    cashAmount: makeNodeComponent("cashAmount"),
    description: makeNodeComponent("description"),
    submit: makeNodeComponent("submit"),
    // Metadata about props expected for PlasmicRequestCard
    internalVariantProps: PlasmicRequestCard__VariantProps,
    internalArgProps: PlasmicRequestCard__ArgProps
  }
);

export default PlasmicRequestCard;
/* prettier-ignore-end */
