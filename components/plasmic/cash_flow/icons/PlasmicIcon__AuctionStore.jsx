// @ts-nocheck
/* eslint-disable */
/* tslint:disable */
/* prettier-ignore-start */
import React from "react";
import { classNames } from "@plasmicapp/react-web";

export function AuctionStoreIcon(props) {
  const { className, style, title, ...restProps } = props;
  return (
    <svg
      xmlns={"http://www.w3.org/2000/svg"}
      fill={"none"}
      viewBox={"0 0 27 27"}
      height={"1em"}
      width={"1em"}
      style={{
        fill: "currentcolor",
        ...(style || {}),
      }}
      className={classNames("cashflow-default__svg", className)}
      {...restProps}
    >
      {title && <title>{title}</title>}

      <path
        d={
          "M7.846 20.168a2.238 2.238 0 00-2.23 2.241c0 1.233.997 2.241 2.23 2.241a2.248 2.248 0 002.242-2.24 2.248 2.248 0 00-2.242-2.242zM1.121 2.241v2.24h2.242l4.035 8.505-1.513 2.745c-.18.314-.28.684-.28 1.076a2.248 2.248 0 002.241 2.24h13.45v-2.24H8.317a.277.277 0 01-.28-.28l.034-.135 1.01-1.826h8.35c.84 0 1.58-.46 1.961-1.154l4.013-7.272a1.09 1.09 0 00.134-.538c0-.616-.504-1.12-1.12-1.12H5.84L4.786 2.24H1.121zm17.934 17.927a2.238 2.238 0 00-2.23 2.241c0 1.233.997 2.241 2.23 2.241a2.248 2.248 0 002.242-2.24 2.248 2.248 0 00-2.242-2.242z"
        }
        fill={"currentColor"}
      ></path>
    </svg>
  );
}

export default AuctionStoreIcon;
/* prettier-ignore-end */
