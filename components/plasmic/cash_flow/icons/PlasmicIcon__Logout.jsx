// @ts-nocheck
/* eslint-disable */
/* tslint:disable */
/* prettier-ignore-start */
import React from "react";
import { classNames } from "@plasmicapp/react-web";

export function LogoutIcon(props) {
  const { className, style, title, ...restProps } = props;
  return (
    <svg
      xmlns={"http://www.w3.org/2000/svg"}
      fill={"none"}
      viewBox={"0 0 27 28"}
      height={"1em"}
      width={"1em"}
      style={{
        fill: "currentcolor",
        ...(style || {}),
      }}
      className={classNames("cashflow-default__svg", className)}
      {...restProps}
    >
      {title && <title>{title}</title>}

      <path
        d={
          "M11.351 18.188l1.586 1.645L18.564 14l-5.625-5.833-1.587 1.645 2.903 3.021H3.375v2.334h10.879l-2.903 3.021zM21.375 3.5H5.625c-1.249 0-2.25 1.05-2.25 2.333V10.5h2.25V5.833h15.75v16.334H5.625V17.5h-2.25v4.667c0 1.283 1.001 2.333 2.25 2.333h15.75c1.238 0 2.25-1.05 2.25-2.333V5.833c0-1.283-1.012-2.333-2.25-2.333z"
        }
        fill={"currentColor"}
      ></path>
    </svg>
  );
}

export default LogoutIcon;
/* prettier-ignore-end */
