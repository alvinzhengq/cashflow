// @ts-nocheck
/* eslint-disable */
/* tslint:disable */
/* prettier-ignore-start */
import React from "react";
import { classNames } from "@plasmicapp/react-web";

export function Rectangle22Icon(props) {
  const { className, style, title, ...restProps } = props;
  return (
    <svg
      xmlns={"http://www.w3.org/2000/svg"}
      fill={"none"}
      viewBox={"0 0 535 355"}
      height={"1em"}
      width={"1em"}
      style={{
        fill: "currentcolor",
        ...(style || {}),
      }}
      className={classNames("cashflow-default__svg", className)}
      {...restProps}
    >
      {title && <title>{title}</title>}

      <path
        d={
          "M0 15C0 6.716 6.716 0 15 0h505c8.284 0 15 6.716 15 15v325c0 8.284-6.716 15-15 15H15c-8.284 0-15-6.716-15-15V15z"
        }
        fill={"currentColor"}
      ></path>
    </svg>
  );
}

export default Rectangle22Icon;
/* prettier-ignore-end */
