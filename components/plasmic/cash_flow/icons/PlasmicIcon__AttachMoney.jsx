// @ts-nocheck
/* eslint-disable */
/* tslint:disable */
/* prettier-ignore-start */
import React from "react";
import { classNames } from "@plasmicapp/react-web";

export function AttachMoneyIcon(props) {
  const { className, style, title, ...restProps } = props;
  return (
    <svg
      xmlns={"http://www.w3.org/2000/svg"}
      fill={"none"}
      viewBox={"0 0 55 56"}
      height={"1em"}
      width={"1em"}
      style={{
        fill: "currentcolor",
        ...(style || {}),
      }}
      className={classNames("cashflow-default__svg", className)}
      {...restProps}
    >
      {title && <title>{title}</title>}

      <path
        d={
          "M27.042 25.433c-5.202-1.376-6.875-2.8-6.875-5.016 0-2.544 2.314-4.317 6.187-4.317 4.08 0 5.592 1.983 5.73 4.9h5.064c-.16-4.013-2.567-7.7-7.356-8.89V7h-6.875v5.04c-4.446.98-8.021 3.92-8.021 8.423 0 5.39 4.377 8.074 10.77 9.637 5.73 1.4 6.876 3.453 6.876 5.623 0 1.61-1.123 4.177-6.188 4.177-4.72 0-6.577-2.147-6.829-4.9h-5.042c.275 5.11 4.034 7.98 8.434 8.937V49h6.875v-5.017c4.468-.863 8.02-3.5 8.02-8.283 0-6.627-5.568-8.89-10.77-10.267z"
        }
        fill={"currentColor"}
      ></path>
    </svg>
  );
}

export default AttachMoneyIcon;
/* prettier-ignore-end */
