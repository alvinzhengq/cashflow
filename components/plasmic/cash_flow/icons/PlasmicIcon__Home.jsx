// @ts-nocheck
/* eslint-disable */
/* tslint:disable */
/* prettier-ignore-start */
import React from "react";
import { classNames } from "@plasmicapp/react-web";

export function HomeIcon(props) {
  const { className, style, title, ...restProps } = props;
  return (
    <svg
      xmlns={"http://www.w3.org/2000/svg"}
      fill={"none"}
      viewBox={"0 0 27 27"}
      height={"1em"}
      width={"1em"}
      style={{
        fill: "currentcolor",
        ...(style || {}),
      }}
      className={classNames("cashflow-default__svg", className)}
      {...restProps}
    >
      {title && <title>{title}</title>}

      <path
        d={
          "M11.209 22.381v-6.714h4.483v6.714h5.605v-8.952h3.362L13.451 3.357 2.24 13.43h3.363v8.952h5.605z"
        }
        fill={"currentColor"}
      ></path>
    </svg>
  );
}

export default HomeIcon;
/* prettier-ignore-end */
