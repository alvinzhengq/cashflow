// @ts-nocheck
/* eslint-disable */
/* tslint:disable */
/* prettier-ignore-start */
import React from "react";
import { classNames } from "@plasmicapp/react-web";

export function VectorIcon(props) {
  const { className, style, title, ...restProps } = props;
  return (
    <svg
      xmlns={"http://www.w3.org/2000/svg"}
      fill={"none"}
      viewBox={"0 0 23 23"}
      height={"1em"}
      width={"1em"}
      style={{
        fill: "currentcolor",
        ...(style || {}),
      }}
      className={classNames("cashflow-default__svg", className)}
      {...restProps}
    >
      {title && <title>{title}</title>}

      <path
        d={
          "M19.966 12.211c.043-.348.072-.71.072-1.085s-.029-.737-.087-1.085l2.458-1.835a.558.558 0 00.146-.71l-2.328-3.852c-.145-.25-.45-.334-.712-.25L16.62 4.506a8.625 8.625 0 00-1.963-1.085L14.221.473A.574.574 0 0013.639 0H8.985a.558.558 0 00-.567.473L7.98 3.42a8.855 8.855 0 00-1.963 1.085L3.124 3.394a.588.588 0 00-.713.25L.084 7.496a.524.524 0 00.145.71l2.458 1.835a6.694 6.694 0 00-.102 1.085c0 .362.03.737.088 1.085L.215 14.047a.558.558 0 00-.146.71l2.327 3.852c.146.25.451.333.713.25l2.894-1.113a8.62 8.62 0 001.964 1.085l.436 2.949c.058.278.29.473.582.473h4.654c.29 0 .538-.195.567-.473l.437-2.949a8.859 8.859 0 001.963-1.085L19.5 18.86c.262.097.567 0 .713-.25l2.327-3.853a.524.524 0 00-.145-.71l-2.43-1.835zM11.312 15.3c-2.4 0-4.363-1.878-4.363-4.173s1.963-4.172 4.363-4.172c2.4 0 4.363 1.877 4.363 4.172S13.712 15.3 11.312 15.3z"
        }
        fill={"currentColor"}
      ></path>
    </svg>
  );
}

export default VectorIcon;
/* prettier-ignore-end */
