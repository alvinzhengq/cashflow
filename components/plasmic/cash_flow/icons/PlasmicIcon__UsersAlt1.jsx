// @ts-nocheck
/* eslint-disable */
/* tslint:disable */
/* prettier-ignore-start */
import React from "react";
import { classNames } from "@plasmicapp/react-web";

export function UsersAlt1Icon(props) {
  const { className, style, title, ...restProps } = props;
  return (
    <svg
      xmlns={"http://www.w3.org/2000/svg"}
      fill={"none"}
      viewBox={"0 0 69 69"}
      height={"1em"}
      width={"1em"}
      style={{
        fill: "currentcolor",
        ...(style || {}),
      }}
      className={classNames("cashflow-default__svg", className)}
      {...restProps}
    >
      {title && <title>{title}</title>}

      <path
        d={
          "M35.362 40.133a14.145 14.145 0 004.888-10.696 14.375 14.375 0 00-28.75 0 14.145 14.145 0 004.887 10.695 23 23 0 00-13.512 20.93 2.875 2.875 0 105.75 0 17.25 17.25 0 1134.5 0 2.875 2.875 0 105.75 0 23 23 0 00-13.513-20.93zm-9.487-2.07a8.624 8.624 0 110-17.25 8.624 8.624 0 010 17.25zm28.002.92a14.375 14.375 0 00-10.752-23.92 2.875 2.875 0 100 5.75 8.625 8.625 0 018.625 8.625 8.625 8.625 0 01-4.313 7.446 2.874 2.874 0 00-1.11 3.819c.226.432.559.8.967 1.068l1.121.748.374.201a20.125 20.125 0 0111.5 18.343 2.875 2.875 0 005.75 0 25.875 25.875 0 00-12.162-22.08z"
        }
        fill={"currentColor"}
      ></path>
    </svg>
  );
}

export default UsersAlt1Icon;
/* prettier-ignore-end */
