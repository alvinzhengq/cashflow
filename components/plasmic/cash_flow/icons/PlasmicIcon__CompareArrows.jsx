// @ts-nocheck
/* eslint-disable */
/* tslint:disable */
/* prettier-ignore-start */
import React from "react";
import { classNames } from "@plasmicapp/react-web";

export function CompareArrowsIcon(props) {
  const { className, style, title, ...restProps } = props;
  return (
    <svg
      xmlns={"http://www.w3.org/2000/svg"}
      fill={"none"}
      viewBox={"0 0 27 27"}
      height={"1em"}
      width={"1em"}
      style={{
        fill: "currentcolor",
        ...(style || {}),
      }}
      className={classNames("cashflow-default__svg", className)}
      {...restProps}
    >
      {title && <title>{title}</title>}

      <path
        d={
          "M10.1 15.363H2.241v2.195h7.857v3.292l4.472-4.39-4.472-4.389v3.292zm6.702-1.097v-3.292h7.857V8.779h-7.857V5.487l-4.472 4.39 4.472 4.389z"
        }
        fill={"currentColor"}
      ></path>
    </svg>
  );
}

export default CompareArrowsIcon;
/* prettier-ignore-end */
