import React from "react";

import TextField from '@mui/material/TextField';
import { withStyles } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';

const styles = {
    root: {
        '& .MuiOutlinedInput-root': {  // - The Input-root, inside the TextField-root
            '& fieldset': {            // - The <fieldset> inside the Input-root
                borderColor: blue[300],   // - Set the Input border
            },
            '&:hover fieldset': {
                borderColor: blue[500], // - Set the Input border when parent has :hover
            },
            '&.Mui-focused fieldset': { // - Set the Input border when parent is focused 
                borderColor: blue[600],
            },

            fontFamily: '"Manrope", "sans-serif"'
        },

        '& .MuiInputLabel-root': {
            fontFamily: '"Manrope", "sans-serif"',
            color: 'rgba(66, 165, 245, 0.8)',
            fontSize: '14px',
            lineHeight: '24px'
        }
    },
};

function StyledTextFieldHigher(props) {
    return <TextField {...props}></TextField>
}

export const StyledTextField = withStyles(styles)(StyledTextFieldHigher)
export default withStyles(styles)(StyledTextFieldHigher)