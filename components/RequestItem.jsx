import React from "react";

import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import ArrowRight from '@mui/icons-material/ArrowRight';
import ChevronRight from '@mui/icons-material/ChevronRight';
import ExpandMore from '@mui/icons-material/ExpandMore';
import ExpandLess from '@mui/icons-material/ExpandLess';
import TextField from '@mui/material/TextField';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Button from '../components/Button'
import blue from '@material-ui/core/colors/blue';

import { deleteRequest, getData, preformTransaction } from '../lib/utils'
import { TRANSACTION_SUCCESS, REQUEST_DEL_FAILED, USER_DOES_NOT_EXIST } from '../lib/constants'

const RequestItemHigher = (props) => {
    const [open, setOpen] = React.useState(false);
    const [amount, setAmount] = React.useState(0);
    const [hidden, setHidden] = React.useState(false);

    const handleClick = () => {
        setOpen(!open);
    };

    return (
        <List sx={{
            "&.MuiButtonBase-root:hover": {
                bgcolor: "transparent"
            }
        }} style={{ width: "25vw", color: blue[600], backgroundColor: 'rgba(0, 0, 0, 0)', "boxShadow": "0px 8px 50px -4px rgba(0, 128, 214, 0.18)", marginTop: '2em', borderRadius: '16px' }} className={hidden ? "hiddenList" : ""}>
            <ListItemButton onClick={handleClick}>
                <ListItemIcon>
                    <ArrowRight style={{ color: blue[600] }} />
                </ListItemIcon>
                <ListItemText primary={props.requestName} />
                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    <ListItem sx={{ pl: 6 }}>
                        <ListItemIcon>
                            <ChevronRight style={{ color: blue[600] }} />
                        </ListItemIcon>
                        <ListItemText primary="Hours of Work Done" secondary={props.cash + " Hours"} />
                    </ListItem>
                    <ListItem sx={{ pl: 6 }}>
                        <ListItemIcon>
                            <ChevronRight style={{ color: blue[600] }} />
                        </ListItemIcon>
                        <ListItemText primary="Request Description" secondary={props.description} />
                    </ListItem>
                    <ListItem sx={{ pl: 6 }}>
                        <ListItemIcon>
                            <ChevronRight style={{ color: blue[600] }} />
                        </ListItemIcon>
                        <TextField label="Amount" type="number" placeholder={1} style={{ width: "70%" }}
                            onKeyPress={(e) => {
                                if (!/[0-9]/.test(e.key)) {
                                    e.preventDefault();
                                }
                            }}

                            onChange={(e) => setAmount(parseInt(e.target.value))}
                        ></TextField>
                    </ListItem>
                    <ListItem sx={{ pl: 6 }}>
                        <ListItemIcon>
                        </ListItemIcon>
                        <Button style={{ paddingRight: "12px" }}
                            onClick={async () => {
                                try {
                                    let res = await preformTransaction(document.cookie
                                        .split('; ')
                                        .find(row => row.startsWith('jwt='))
                                        .split('=')[1], props.requestName, parseInt(props.cash)*10)
                                    if (res != TRANSACTION_SUCCESS) {
                                        props.add(
                                            <Alert variant="standard" severity="error" style={{ margin: "1em" }}>
                                                <AlertTitle>Transaction Unsuccessful, Please Check your Balance</AlertTitle>
                                            </Alert>
                                        )

                                        return
                                    }
                                } catch (err) {
                                    props.add(
                                        <Alert variant="standard" severity="error" style={{ margin: "1em" }}>
                                            <AlertTitle>Transaction Unsuccessful, Please Check your Balance</AlertTitle>
                                        </Alert>
                                    )

                                    return
                                }


                                setHidden(true); setOpen(false)
                                setTimeout(async () => {
                                    await deleteRequest(document.cookie
                                        .split('; ')
                                        .find(row => row.startsWith('jwt='))
                                        .split('=')[1], props.id)
                                    let newData = await getData(document.cookie
                                        .split('; ')
                                        .find(row => row.startsWith('jwt='))
                                        .split('=')[1])
                                    props.setUserData(newData)
                                    setHidden(false);
                                }, 400)
                            }}
                        >Send 10x Hrs</Button>

                        <Button style={{ paddingRight: "12px" }}
                            onClick={async () => {
                                if (isNaN(amount) || amount == 0) {
                                    props.add(
                                        <Alert variant="standard" severity="error" style={{ margin: "1em" }}>
                                            <AlertTitle>Invalid Amount Specified</AlertTitle>
                                        </Alert>
                                    )

                                    return
                                }

                                try {
                                    let res = await preformTransaction(document.cookie
                                        .split('; ')
                                        .find(row => row.startsWith('jwt='))
                                        .split('=')[1], props.requestName, amount)
                                    if (res != TRANSACTION_SUCCESS) {
                                        props.add(
                                            <Alert variant="standard" severity="error" style={{ margin: "1em" }}>
                                                <AlertTitle>Transaction Unsuccessful, Please Check your Balance</AlertTitle>
                                            </Alert>
                                        )

                                        return
                                    }
                                } catch (err) {
                                    props.add(
                                        <Alert variant="standard" severity="error" style={{ margin: "1em" }}>
                                            <AlertTitle>Transaction Unsuccessful, Please Check your Balance</AlertTitle>
                                        </Alert>
                                    )

                                    return
                                }


                                setHidden(true); setOpen(false)
                                setTimeout(async () => {
                                    await deleteRequest(document.cookie
                                        .split('; ')
                                        .find(row => row.startsWith('jwt='))
                                        .split('=')[1], props.id)
                                    let newData = await getData(document.cookie
                                        .split('; ')
                                        .find(row => row.startsWith('jwt='))
                                        .split('=')[1])
                                    props.setUserData(newData)
                                    setHidden(false);
                                }, 400)
                            }}
                        >Send</Button>

                        <Button danger
                            onClick={async () => {
                                setHidden(true); setOpen(false)
                                setTimeout(async () => {
                                    await deleteRequest(document.cookie
                                        .split('; ')
                                        .find(row => row.startsWith('jwt='))
                                        .split('=')[1], props.id)
                                    let newData = await getData(document.cookie
                                        .split('; ')
                                        .find(row => row.startsWith('jwt='))
                                        .split('=')[1])
                                    props.setUserData(newData)
                                    setHidden(false);
                                }, 400)
                            }}
                        >Deny</Button>
                    </ListItem>
                </List>
            </Collapse>
        </List >
    )
}

export const RequestItem = RequestItemHigher
export default RequestItemHigher