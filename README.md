# CashFlow

Full-stack banking/eCommerce application with a fully functional JWT based authentication system, real time transactions, and transaction requests. Made using Next.js, and MongoDB.
